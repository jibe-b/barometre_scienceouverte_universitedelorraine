Les requêtes suivantes ont été saisies dans les différentes bases de données pour obtenir les listes de publications par année :

Web of Science
Affiliation : Université de Lorraine

PubMed
("Université de Lorraine"[Affiliation] OR "University of Lorraine"[Affiliation] OR "lorraine loria"[Affiliation] OR "lorraine metz"[Affiliation] 
OR "lorraine nancy"[Affiliation] OR "lorraine nancy university"[Affiliation]OR "lorraine umr"[Affiliation] OR "lorraine umr 1121"[Affiliation] 
OR "lorraine univ"[Affiliation] OR "lorraine univeristy"[Affiliation] OR "lorraine universite"[Affiliation] OR "lorraine universite bp"[Affiliation] 
OR "lorraine universite bp 230"[Affiliation] OR "lorraine universite interactions"[Affiliation] OR "lorraine universite interactions arbres"[Affiliation] 
OR "lorraine universite interactions arbres/microorganismes"[Affiliation] OR "lorraine universite interactions arbres microorganismes"[Affiliation] 
OR "lorraine university"[Affiliation] OR "lorraine university and"[Affiliation] OR "lorraine university and cnrs"[Affiliation] OR 
"lorraine university and cnrs unr"[Affiliation] OR "lorraine university and cnrs unr 7365"[Affiliation] OR 
"lorraine university and cnrs unr 7561"[Affiliation] OR "lorraine university cnrs"[Affiliation] OR "lorraine university cnrs umr"[Affiliation] OR 
"lorraine university cnrs umr 7198"[Affiliation] OR "lorraine university hospital"[Affiliation] OR "lorraine university hospital centre"[Affiliation] 
OR "lorraine university medical"[Affiliation] OR "lorraine university medical school"[Affiliation] OR "lorraine university vandoeuvre"[Affiliation] 
OR "lorraine university vandoeuvre les"[Affiliation] OR "lorraine university vandoeuvre les nancy"[Affiliation] OR "lorraine, nancy"[Affiliation] 
OR "lorraine, nancy, france"[Affiliation] OR "lorraine, umr"[Affiliation] OR "lorrainevandoeuvre"[Affiliation] OR 
"lorrainevandoeuvre les nancy"[Affiliation]) AND ("2016"[Date - Publication] : "2016"[Date - Publication])

HAL-UL
Toute la collection HAL-UL via ExtrHAL

APC
https://zenodo.org/record/3491721#.Xkqrf0pCdPY

Lens.org
Institution : University of Lorraine